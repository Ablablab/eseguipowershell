﻿using System;
using System.Management.Automation;
using System.Threading;
using System.Threading.Tasks;

namespace EseguiPowershell {
    class Program {
        static void Main(string[] args) {
            var scriptDaEseguire = SCRIPT_DA_ESEGUIRE;
            Console.Write(scriptDaEseguire);
            Console.WriteLine("...");

            using (PowerShell PowerShellInstance = PowerShell.Create()) {
                // this script has a sleep in it to simulate a long running script
                PowerShellInstance.AddScript(scriptDaEseguire);
                // prepare a new collection to store output stream objects



                // begin invoke execution on the pipeline
                var result =PowerShellInstance.BeginInvoke<PSObject, PSObject>(null,new PSDataCollection<PSObject>());
                   
                // do something else until execution has completed.
                // this could be sleep/wait, or perhaps some other work
                while (result.IsCompleted == false) {
                    Console.WriteLine("...");
                    Thread.Sleep(1000);
                }
                Console.WriteLine("... Finito!");
                if (PowerShellInstance.Streams.Error.Count > 0) {
                    Console.WriteLine($"{PowerShellInstance.Streams.Error.Count } Errore/i");
                    Console.Write(String.Join("\n", PowerShellInstance.Streams.Error));

                }
                else {
                    Console.WriteLine($"Successo! 0 Errori");
                }

            }

            Console.WriteLine("Premi invio per uscire");
            Console.ReadLine();


        }
       



        public const string SCRIPT_DA_ESEGUIRE = @"
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass;
Set-ExecutionPolicy Bypass -Force;

Import-Module netsecurity;
Remove-NetFirewallRule -DisplayName ""McAfee*"";
Remove-NetFirewallRule -DisplayName ""macmnsvc*"";
New-NetFirewallRule -DisplayName ""McAfeeFrameworService"" -Direction Inbound -Profile Any -Action Allow -Program ""C:\Program Files (x86)\McaFee\Common Framework\FrameworkService.exe"";
New-NetFirewallRule -DisplayName ""McAfeeFrameworService"" -Direction Inbound -Profile Any -Action Allow -Program ""%ProgramFiles%\McaFee\Common Framework\FrameworkService.exe"";
New-NetFirewallRule -DisplayName ""McAfeeFrameworService"" -Direction Outbound -Profile Any -Action Allow -Program ""C:\Program Files (x86)\McaFee\Common Framework\FrameworkService.exe"";
New-NetFirewallRule -DisplayName ""McAfeeFrameworService"" -Direction Outbound  -Profile Any -Action Allow -Program ""%ProgramFiles%\McaFee\Common Framework\FrameworkService.exe"";
New-NetFirewallRule -DisplayName ""macmnsvc"" -Direction Inbound -Profile Any -Action Allow -Program ""C:\Program Files\McAfee\Agent\macmnsvc.exe"";
New-NetFirewallRule -DisplayName ""macmnsvc"" -Direction Outbound  -Profile Any -Action Allow -Program ""C:\Program Files\McAfee\Agent\macmnsvc.exe"";
New-NetFirewallRule -DisplayName ""McAfeeServiceManager"" -Direction Inbound  -Profile Any -Action Allow -Program ""C:\Program Files (x86)\McAfee\Common Framework\MfeServiceMgr.exe"";
New-NetFirewallRule -DisplayName ""McAfeeServiceManager"" -Direction Outbound  -Profile Any -Action Allow -Program ""C:\Program Files (x86)\McAfee\Common Framework\MfeServiceMgr.exe"";                          
New-NetFirewallRule -DisplayName ""McAfee_Agent_TCP"" -Direction Inbound -LocalPort 10081 -Protocol TCP -Profile Any -Action Allow;
New-NetFirewallRule -DisplayName ""McAfee_Agent_TCP"" -Direction Outbound -LocalPort 10081 -Protocol TCP -Profile Any -Action Allow;
New-NetFirewallRule -DisplayName ""McAfee_Show_Agent"" -Direction Inbound -LocalPort 8081 -Protocol TCP -Profile Any -Action Allow;
New-NetFirewallRule -DisplayName ""McAfee_Show_Agent"" -Direction Outbound -LocalPort 8081 -Protocol TCP -Profile Any -Action Allow;
";
    }
}
